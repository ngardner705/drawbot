# ###############################################
# 
# DrawBot
# This library will include the following featues
#   XY Control
#   Pen Control
#
# Nathan Gardner nathan@factory8.com
# March 2017
#
# ###############################################
#
# Hardware requirements
# Servo
# - need to lookup what im using...
#
# Motors
# 2 Stepper motors with a standard interface
# 28BYJ-48 stepper motor
# ULN2003 chip driver
#
# ###############################################

# Pass in GPIO object, and pinConfig dictionary
# DrawBot(GPIO,pinConfig)
#
# pinConfig = {
#    [pen] = (int) GPIO pin
#    [motorLeft] = (array) GPIO pins
#    [motorRight] = (array) GPIO pins
# }
# PINS MUST USE BCM GPIO NUMBERS!

class DrawBot(object):

    # Constructor
    def __init__(self,GPIO,pinConfig):
        self.LoopSpeed = 0.002
        self.Pen = DrawBotPen(GPIO,pinConfig.pen)
        self.MotorLeft = DrawBotMotor(GPIO,pinConfig.motorLeft)
        self.MotorRight = DrawBotMotor(GPIO,pinConfig.motorRight)
    
    # Call this every loop
    def main(self):
        
        # Step the motors as needed
        # Left Motor
        self.MotorLeft.step()
        self.MotorLeft.step()
        
        # Wait before moving on
        time.sleep(self.LoopSpeed)
    
    # stop moving XY
    def stopMoving(self):
        self.MotorLeft.setDirection(0)
        self.MotorRight.setDirection(0)
    
    # Move Up
    def moveUp(self):
        self.MotorLeft.setDirection(-1)
        self.MotorRight.setRirection(1)
    
    # Move Down
    def moveDown():
        self.MotorLeft.setDirection(1)
        self.MotorRight.setDirection(-1)
    
    # Move Left
    def moveLeft():
        self.MotorLeft.setDirection(1)
        self.MotorRight.setDirection(1)
    
    # Move Right
    def moveRight():
        self.MotorLeft.setDirection(-1)
        self.MotorRight.setDirection(-1)
    
    # Pen Up
    def penUp():
        self.Pen.up()
    
    # Pen Down
    def penDown():
        self.Pen.down()
    

class DrawBotPen(object):
    
    def __init__(self, GPIO, pin):
        # setup pin
        self.pin = pin
        GPIO.setup(self.pin,GPIO.OUT)
        
        # set to PWM
        self.pen = GPIO.PWM(self.pin,50)
        
        # set pen positions for servo
        # can these be optionally in the config?
        self.upPosition = 8
        self.downPositionStart = 10.5
        self.downPositionEnd = 11.5
        self.position = self.upPosition
    
    def powerOn(self):        
        self.pen.stop()
        self.pen = GPIO.PWM(self.pin,50)
        self.pen.start(self.position)
        
    def powerOff(self):
        self.pen.stop()
    
    def up(self):
        
        self.powerOn()
        
        ## move up a bit
        self.position -= .1
        
        ## if above drawing positions, move all the way up
        if self.position <= self.downPositionStart:
            self.position = self.upPosition
        
        self.pen.ChangeDutyCycle(self.position)
        
        # wait for it to finish
        time.sleep(0.25)
        
        self.powerOff()
    
    def down(self):
        
        ## turn it on
        self.powerOn()
        
        ## move down a bit
        self.position += .1
        
        ## if above drawing positions, move down to start of drawing
        if self.position <= self.downPositionStart:
            self.position = self.downPositionStart
      
        ## if trying to go too far, prevent
        if self.position >= self.downPositionEnd:
            self.position = self.downPositionEnd
        
        self.pen.ChangeDutyCycle(self.position)
        
        # wait for it to finish
        time.sleep(0.25)
        
        self.powerOff()
    

class DrawBotMotor(object):
    
    def __init__(self, GPIO, pins):
        # setup pins
        self.pins = pins
        for pin in self.pins:
            GPIO.setup(pin,GPIO.OUT)
            GPIO.output(pin, False)
        
        # motor sequence
        self.stepSequence = [[1,0,0,1],
            [1,1,0,0],
            [0,1,1,0],
            [0,0,1,1]]
        self.stepSequencePosition = 0
        self.stepCounter = 0
        
        # 1 = Clockwise, -1 = Counter Clockwise, 0 = Off
        self.direction = 0
        
        # Speed
        self.speed = 0.002
    
    def setDirection(self,direction):
        self.direction = direction
        
    def setSpeed(self,speed):
        #dunno how todo this yet
        self.speed = 0.002
        
    def step(self):
        
        # do we have a direction to step
        if(self.direction != 0):
            
            # We have to set 4 pins to take a step
            for pinIndex in range(0, 4):
                
                # get the GPIO pin number
                pin = self.pins[pinIndex]
                if self.stepSequence[self.stepSequencePosition][pinIndex]!=0:
                    GPIO.output(pin, True)
                else:
                    GPIO.output(pin, False)
            
            # Count the step
            self.stepCounter += self.direction
            
            # If we reach the end of the sequence, start again
            if (self.stepSequencePosition >= len(self.stepSequence)):
                self.stepSequencePosition = 0
            if (self.stepSequencePosition <= -len(self.stepSequence)):
                self.stepSequencePosition = 0
    


