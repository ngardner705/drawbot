#!/usr/bin/python
#--------------------------------------
# DrawBot Manual controls
# For testing, and fun!
# Control DrawBot with arrow keys
#
# Nathan Gardner nathan@factory8.com
# Feb 2017
#
#--------------------------------------

# Import required libraries
import sys
import time
import curses
import RPi.GPIO as GPIO


# Setup curses - screen and keyboard input
stdscr = curses.initscr()
curses.noecho() # dont show user input
curses.cbreak() # read input as they type, not with enter

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

# Pen Up/Down Servo
PenUpDownPin = 12
PenUpPosition = 8
PenDownPositionStart = 10.5
PenDownPositionEnd = 11.5
PenPosition = PenUpPosition
GPIO.setup(PenUpDownPin,GPIO.OUT)
PenServo = GPIO.PWM(PenUpDownPin,50)

# Left Motor
# Define GPIO signals to use
MotorLeftPins = [17,18,27,22]
for pin in MotorLeftPins:
  stdscr.addstr("Seting up Left Motor pin %i" %(pin))
  GPIO.setup(pin,GPIO.OUT)
  GPIO.output(pin, False)

# Right Motor
# Define GPIO signals to use
MotorRightPins = [5,6,13,19]
for pin in MotorRightPins:
  stdscr.addstr("Seting up Right Motor pin %i" %(pin))
  GPIO.setup(pin,GPIO.OUT)
  GPIO.output(pin, False)

stdscr.addstr("Motors Setup")

# Motor move sequence - for pins
MotorSeq = [[1,0,0,1],
       [1,1,0,0],
       [0,1,1,0],
       [0,0,1,1]]

SeqForStep = len(MotorSeq) # How many pin changes count as a "step"
SeqCounterLeft = 0
SeqCounterRight = 0

# 1 = Clockwise, -1 = Counter Clockwise
StepDirLM = 0
StepDirRM = 0

# Setup Motor speeds.
# 1st gear = 0.010
# 2nd gear = 0.008
# 3rd gear = 0.006
# 4th gear = 0.004
# 5th gear = 0.002

FullSpeed = 0.002
NormalSpeed = 0.004
HalfSpeed = 0.008

# Loop States
stateStop = 'stop'
stateLeft = 'left'
stateRight = 'right'
stateUp = 'up'
stateDown ='down'
stateContinue = 'continue'
loopState = stateStop

### CONTROL 2 MOTORS, AT DIFFERENT SPEEDS
## TEMP CODE ##
LoopSpeed = 0.001
NormalSpeedLoops = 4
HalfSpeedLoops = 8

# Motor setup for movements
def stopMoving():
  global StepDirLM
  global StepDirRM
  stdscr.addstr("STOPPED!!")
  StepDirLM = 0
  StepDirRM = 0
  stdscr.nodelay(False)
  return

def moveUp():
  global StepDirLM
  global StepDirRM
  stdscr.addstr("UP")
  StepDirLM = -1
  StepDirRM = 1
  stdscr.nodelay(True)
  return

def moveDown():
  global StepDirLM
  global StepDirRM
  stdscr.addstr("DOWN")
  StepDirLM = 1
  StepDirRM = -1
  stdscr.nodelay(True)
  return

def moveLeft():
  global StepDirLM
  global StepDirRM
  stdscr.addstr("DOWN")
  StepDirLM = 1
  StepDirRM = 1
  stdscr.nodelay(True)
  return

def moveRight():
  global StepDirLM
  global StepDirRM
  StepDirLM = -1
  StepDirRM = -1
  stdscr.nodelay(True)
  return

def penUp():
  global PenPosition
  global PenServo

  penOn()

  ## move up a bit
  PenPosition -= .1

  ## if above drawing positions, move all the way up
  if PenPosition <= PenDownPositionStart:
    PenPosition = PenUpPosition

  PenServo.ChangeDutyCycle(PenPosition)
  time.sleep(0.5)
  penOff()
  return

def penDown():
  global PenPosition
  global PenServo
  
  penOn()

  ## move down a bit
  PenPosition += .1

  ## if above drawing positions, move down to start of drawing
  if PenPosition <= PenDownPositionStart:
    PenPosition = PenDownPositionStart

  ## if trying to go too far, prevent
  if PenPosition >= PenDownPositionEnd:
    PenPosition = PenDownPositionEnd
  
  PenServo.ChangeDutyCycle(PenPosition)
  time.sleep(0.5)
  penOff()
  return

def penOn():
  global PenServo

  PenServo.stop()
  PenServo = GPIO.PWM(PenUpDownPin,50)
  PenServo.start(PenPosition)

def penOff():
  global PenServo

  PenServo.stop()

# Start pen in up position
penOn()
penUp()
penOff()

# Start main loop
while True:
  
  # Get the users last input
  userInput = stdscr.getch()

  # If the input a command, do it
  if userInput == ord('w'):
    moveUp()
  if userInput == ord("s"):
    moveDown()
  if userInput == ord("a"):
    moveLeft()
  if userInput == ord("d"):
    moveRight()
  if userInput == ord(" "):
    stopMoving()
  if userInput == ord("-"):
    penUp()
  if userInput == ord("+"):
    penDown()
  if userInput == ord("/"):
    penOn()
  if userInput == ord("*"):
    penOff()

  # Step the motors as needed
  for pin in range(0, 4):
    # Left motor step
    pinLeft = MotorLeftPins[pin]
    if MotorSeq[SeqCounterLeft][pin]!=0:
      GPIO.output(pinLeft, True)
    else:
      GPIO.output(pinLeft, False)
    # Right motor step
    pinRight = MotorRightPins[pin]
    if MotorSeq[SeqCounterRight][pin]!=0:
      GPIO.output(pinRight, True)
    else:
      GPIO.output(pinRight, False)

  SeqCounterLeft += StepDirLM
  SeqCounterRight += StepDirRM

  # If we reach the end of the sequence
  # start again
  if (SeqCounterLeft>=SeqForStep):
   SeqCounterLeft = 0
  if (SeqCounterLeft<=-SeqForStep):
    SeqCounterLeft = 0
  if (SeqCounterRight>=SeqForStep):
   SeqCounterRight = 0
  if (SeqCounterRight<=-SeqForStep):
    SeqCounterRight = 0
    
  # Wait before moving on
  time.sleep(FullSpeed)
