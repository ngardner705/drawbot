#!/usr/bin/python
# or this? ! /usr/bin/env python3
#--------------------------------------
# DrawBot Manual controls
# For testing, and fun!
# Control DrawBot with arrow keys
#
# Nathan Gardner nathan@factory8.com
# March 2017
#
#--------------------------------------

# Import required libraries
import sys
import time
import curses
import RPi.GPIO as GPIO

# Setup curses - screen and keyboard input
stdscr = curses.initscr()
curses.noecho() # dont show user input
curses.cbreak() # read input as they type, not with enter

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

# Define BCM GPIO pins for pen and motors
PenPin = 12
MotorLeftPins = [17,18,27,22]
MotorRightPins = [5,6,13,19]

# this is necessary for motors
LoopSpeed = 0.002

# Start main loop
while True:
  
  # Get the users last input
  userInput = stdscr.getch()