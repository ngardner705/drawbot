#!/usr/bin/python
# or this? ! /usr/bin/env python3
#--------------------------------------
# DrawBot Manual controls
# For testing, and fun!
# Control DrawBot with arrow keys
#
# Nathan Gardner nathan@factory8.com
# March 2017
#
#--------------------------------------

# Import required libraries
import sys
import time
import curses
import RPi.GPIO as GPIO
import drawbot as DrawBot

# Setup curses - screen and keyboard input
stdscr = curses.initscr()
curses.noecho() # dont show user input
curses.cbreak() # read input as they type, not with enter

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

# Define BCM GPIO pins for pen and motors
pinConfig = dict()
pinConfig.pen = 12
pinConfig.motorLeft = [17,18,27,22]
pinConfig.motorRight = [5,6,13,19]

# DrawBot
drawer = DrawBot(GPIO,pinConfig)

# Start main loop
while True:
    
    # Get the users last input
    userInput = stdscr.getch()
    
    # If the input a command, do it
    if userInput == ord('w'):
        stdscr.addstr("UP")
        drawer.moveUp()
    
    if userInput == ord("s"):
        stdscr.addstr("DOWN")
        drawer.moveDown()
    
    if userInput == ord("a"):
        stdscr.addstr("LEFT")
        drawer.moveLeft()
    
    if userInput == ord("d"):
        stdscr.addstr("RIGHT")
        drawer.moveRight()
    
    if userInput == ord(" "):
        stdscr.addstr("STOP")
        drawer.stopMoving()
    
    if userInput == ord("-"):
        stdscr.addstr("PEN - UP")
        drawer.penUp()

    if userInput == ord("+"):
        stdscr.addstr("PEN - DOWN")
        drawer.penUp()
    
    
    # Let DrawBot do its thing
    drawer.main()

  

