#!/usr/bin/python
#--------------------------------------
# Servo testing
#
# Nathan Gardner nathan@factory8.com
# Feb 2017
#
#--------------------------------------

# Import required libraries
import sys
import time
import curses
import RPi.GPIO as GPIO

# Setup curses - screen and keyboard input
stdscr = curses.initscr()
curses.noecho() # dont show user input
curses.cbreak() # read input as they type, not with enter

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

# Pen Up/Down Servo
PenUpDownPin = 12
PenUpPosition = 8
PenDownPositionStart = 7.9
PenDownPositionEnd = 11.5
PenPosition = PenUpPosition
GPIO.setup(PenUpDownPin,GPIO.OUT)
PenServo = GPIO.PWM(PenUpDownPin,50)
PenServo.start(PenUpPosition)

# Servo Testing
freq = 50

def penUp():
  global PenPosition

  ## move up a bit
  PenPosition -= .1

  ## if above drawing positions, move all the way up
  if PenPosition <= PenDownPositionStart:
    PenPosition = PenUpPosition

  PenServo.ChangeDutyCycle(PenPosition)
  return

def penDown():
  global PenPosition
  
  ## move down a bit
  PenPosition += .1

  ## if above drawing positions, move down to start of drawing
  if PenPosition <= PenDownPositionStart:
    PenPosition = PenDownPositionStart

  ## if trying to go too far, prevent
  if PenPosition >= PenDownPositionEnd:
    PenPosition = PenDownPositionEnd
  
  PenServo.ChangeDutyCycle(PenPosition)
  return


# Start main loop
while True:
  
  # Get the users last input
  userInput = stdscr.getch()

  # If the input a command, do it
  if userInput == ord("-"):
    penUp()
  if userInput == ord("+"):
    penDown()

  if userInput == ord("]"):
    freq += 10
    stdscr.addstr("Frequency set to %i - \"\ \ \" to commit" %(freq))
  if userInput == ord("["):
    freq -= 10
    stdscr.addstr("Frequency set to %i - \"\ \ \" to commit" %(freq))
  if userInput == ord("\\"):
    PenServo.ChangeFrequency(freq)
    stdscr.addstr("Frequency is now %i " %(freq))
  if userInput == ord(" "):
    PenServo.stop()
  if userInput == ord("1"):
    PenServo.stop()
    PenServo = GPIO.PWM(PenUpDownPin,freq)
    PenServo.start(PenPosition)




    
